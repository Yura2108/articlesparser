# ArticlesParser

## Работает на:
- Java 21
- Jsoup 1.17.2 (lib)
- RabbitMQ 5.21.0 (lib)
- Elasticsearch 8.13.4
- RssReader 3.6.0 (lib)


## Описание кода
- 3 сервиса, работающих с брокером сообщений RabbitMQ
- 2 дата-класса: Article (информация о статье) и LinkData (ссылка и заголовок статьи с хэшем)
- 2 класса-утилиты. Мост для работы с ElasticSearch, утилита для работы с RabbitMQ
- Конфиг для настроек подключений

## Описание структур данных
#### Article
Автор
Название
Ссылка
Дата публикации
Текст статьи

#### LinkData
Ссылка
Заголовок
Хэш


## Очереди для RabbitMQ
- Links
- Pages

### Первый сервис для получения ссылок и заголовков статей

Работает по следующему принципу:
- Через библиотеку RSSReader из ссылки на RSS получает список статей и их заголовков.
- Передаёт в очередь RabbitMQ - Links

### Второй сервис для обработки ссылок из очереди (Links)

Работает по принципу:
- Получение ссылки из очереди Links путём регистрации Consumer`a и установки ручного подтверждения
- Обработка страницы статьи: получение текста, автора, даты публикации и необходимой информации
- Передаёт собранную информацию в очередь RabbitMQ - Pages

### Третий сервис для обработки полученных данных и передачи их в базу ElasticSearch

Работает по принципу:
- Получение данных о статье путём `basic.Get`
- Передача данных в базу ElasticSearch



## Запуск RabbitMQ

Вставить скрипт в docker-compose.yaml файл.
После этого на вашем сервере на порту 15672 будет доступна админка

Credentials: `rabbitmq`:`rabbitmq`
```yaml
version: '3'

services:
    rabbitmq:
      image: "rabbitmq:3-management"
      hostname: "rabbitmq"
      environment:
        RABBITMQ_ERLANG_COOKIE: "SWQOKODSQALRPCLNMEQG"
        RABBITMQ_DEFAULT_USER: "rabbitmq"
        RABBITMQ_DEFAULT_PASS: "rabbitmq"
        RABBITMQ_DEFAULT_VHOST: "/"
      volumes:
        - ./docker-data/rabbitmq/etc/definitions.json:/etc/rabbitmq/definitions.json
        - ./docker-data/rabbitmq/etc/rabbitmq.conf:/etc/rabbitmq/rabbitmq.conf
        - ./docker-data/rabbitmq/data:/var/lib/rabbitmq/mnesia/rabbit@my-rabbit
        - ./docker-data/rabbitmq/logs:/var/log/rabbitmq/log
      ports:
        - "15672:15672"
        - "5672:5672"
```

## Запуск ElasticSearch

```yaml
version: '3'
services:
  elasticsearch:
    image: docker.elastic.co/elasticsearch/elasticsearch:7.17.0
    container_name: elasticsearch
    environment:
      - discovery.type=single-node
      - "ES_JAVA_OPTS=-Xms512m -Xmx512m"
    ulimits:
      memlock:
        soft: -1
        hard: -1
    volumes:
      - esdata:/usr/share/elasticsearch/data
    ports:
      - 9200:9200
    networks:
      - esnet

volumes:
  esdata:
    driver: local

networks:
  esnet:
    driver: bridge
```