plugins {
    id("java")
}

group = "me.dasha"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.apache.logging.log4j:log4j-api:2.23.1")
    implementation("org.apache.logging.log4j:log4j-core:2.23.1")
    implementation("co.elastic.clients:elasticsearch-java:8.13.4")
    implementation("com.fasterxml.jackson.core:jackson-databind:2.17.0")
    implementation("org.json:json:20240303")
    implementation("com.rabbitmq:amqp-client:5.21.0")
    implementation("com.apptasticsoftware:rssreader:3.6.0")
    implementation("org.jsoup:jsoup:1.17.2")
    testImplementation(platform("org.junit:junit-bom:5.10.0"))
    testImplementation("org.junit.jupiter:junit-jupiter")
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<JavaCompile>
{
    options.encoding = "UTF-8"
}