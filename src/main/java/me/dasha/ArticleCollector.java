package me.dasha;

import com.rabbitmq.client.ConnectionFactory;
import me.dasha.services.ElasticWrapper;
import me.dasha.services.LinkParser;
import me.dasha.services.PageParser;
import me.dasha.utils.ElasticBridge;
import me.dasha.utils.RabbitUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ArticleCollector
{
    private static final Logger logger = LoggerFactory.getLogger(ArticleCollector.class);

    public static void main(String[] args) throws IOException {

        // 1. Загрузка конфигурации
        Config config = Config.load("config.properties");

        // 2. Инициализация компонентов
        ConnectionFactory rabbitFactory = new ConnectionFactory();
        rabbitFactory.setHost(config.getRabbitHost());
        rabbitFactory.setUsername(config.getRabbitUsername());
        rabbitFactory.setPassword(config.getRabbitPassword());

        try(
                var connection = rabbitFactory.newConnection();
                var channel = connection.createChannel()
                ) {
            channel.queueDeclare(RabbitUtils.LINK_MQ, false, false, false, null);
            channel.queueDeclare(RabbitUtils.PAGE_MQ, false, false, false, null);
        }catch (Exception e) {
            e.printStackTrace();
        }

        ElasticBridge elasticBridge = new ElasticBridge(config.getElasticUrl(), config.getElasticIndex());
        elasticBridge.createIndex();

        List<String> rssUrls = config.getRssUrls();

        // 3. Запуск парсеров RSS-лент
        ExecutorService executor = Executors.newFixedThreadPool(rssUrls.size());
        for (String rssUrl : rssUrls) {
            executor.execute(new LinkParser(rssUrl, rabbitFactory));
        }

        // 4. Запуск парсера страниц
        executor.execute(new PageParser(rabbitFactory));

        // 5. Запуск обработчика ElasticSearch
        executor.execute(new ElasticWrapper(rabbitFactory, elasticBridge));

        logger.info("Article Collector started.");

        // 6. Ожидание завершения работы
        executor.shutdown();
        while (!executor.isTerminated()) {
            // Можно добавить логику ожидания или мониторинга
        }

        logger.info("Article Collector stopped.");
    }
}