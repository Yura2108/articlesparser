package me.dasha;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class Config {

    private static final Logger logger = LoggerFactory.getLogger(Config.class);

    private final String rabbitHost;
    private final String rabbitUsername;
    private final String rabbitPassword;
    private final String elasticUrl;
    private final String elasticIndex;
    private final List<String> rssUrls;

    private Config(String rabbitHost, String rabbitUsername, String rabbitPassword, String elasticUrl, String elasticIndex, List<String> rssUrls) {
        this.rabbitHost = rabbitHost;
        this.rabbitUsername = rabbitUsername;
        this.rabbitPassword = rabbitPassword;
        this.elasticUrl = elasticUrl;
        this.elasticIndex = elasticIndex;
        this.rssUrls = rssUrls;
    }

    public static Config load(String configFilePath) throws IOException {
        Properties prop = new Properties();
        try (InputStream input = new FileInputStream(configFilePath)) {
            prop.load(input);
        }

        String rabbitHost = prop.getProperty("rabbit.host");
        String rabbitUsername = prop.getProperty("rabbit.username");
        String rabbitPassword = prop.getProperty("rabbit.password");
        String elasticUrl = prop.getProperty("elastic.url");
        String elasticIndex = prop.getProperty("elastic.index");
        List<String> rssUrls = Arrays.asList(prop.getProperty("rss.urls").split(","));

        logger.info("Configuration loaded successfully.");
        return new Config(rabbitHost, rabbitUsername, rabbitPassword, elasticUrl, elasticIndex, rssUrls);
    }

    public String getRabbitHost() {
        return rabbitHost;
    }

    public String getRabbitUsername() {
        return rabbitUsername;
    }

    public String getRabbitPassword() {
        return rabbitPassword;
    }

    public String getElasticUrl() {
        return elasticUrl;
    }

    public String getElasticIndex() {
        return elasticIndex;
    }

    public List<String> getRssUrls() {
        return rssUrls;
    }
}