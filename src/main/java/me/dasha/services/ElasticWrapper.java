package me.dasha.services;

import com.rabbitmq.client.ConnectionFactory;
import me.dasha.storage.Article;
import me.dasha.utils.ElasticBridge;
import me.dasha.utils.RabbitUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class ElasticWrapper implements Runnable
{
    private final ConnectionFactory factory;
    private final ElasticBridge elasticBridge;
    private final Logger logger = LoggerFactory.getLogger(ElasticWrapper.class);

    public ElasticWrapper(ConnectionFactory factory, ElasticBridge elasticBridge)
    {
        this.factory = factory;
        this.elasticBridge = elasticBridge;
    }

    @Override
    public void run()
    {
        try
        (
            var conn = factory.newConnection();
            var channel = conn.createChannel();
        )
        {
            while (true)
            {
                if (channel.messageCount(RabbitUtils.PAGE_MQ) == 0) continue;
                var input = channel.basicGet(RabbitUtils.PAGE_MQ, true);
                Article article = Article.fromJson(new String(input.getBody()));
                if (elasticBridge.checkExistence(article.getHash())) {return;}
                elasticBridge.insertData(article);
            }
        }
        catch (TimeoutException | IOException e)
        {
            logger.info("123");
            throw new RuntimeException(e);
        }
    }
}