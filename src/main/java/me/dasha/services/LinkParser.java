package me.dasha.services;

import com.apptasticsoftware.rssreader.RssReader;
import com.rabbitmq.client.ConnectionFactory;
import me.dasha.storage.LinkData;
import me.dasha.utils.RabbitUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

public class LinkParser implements Runnable
{
    private final String url;
    private final ConnectionFactory factory;

    public LinkParser(String url, ConnectionFactory factory)
    {
        this.url = url;
        this.factory = factory;
    }

    @Override
    public void run()
    {
        RssReader rssReader = new RssReader();
        try
        (
            var conn = factory.newConnection();
            var channel = conn.createChannel();
        )
        {
            rssReader.read(url).forEach(
                item ->
                {
                    LinkData linkData = new LinkData(
                            item.getLink().orElse(""),
                            item.getTitle().orElse("Интересная статья")
                    );
                    try
                    {
                        channel.basicPublish
                        (
                            "",
                            RabbitUtils.LINK_MQ,
                            null,
                            linkData.toString().getBytes(StandardCharsets.UTF_8)
                        );
                    }
                    catch (IOException e)
                    {
                        throw new RuntimeException(e);
                    }
                }
            );
        }
        catch (TimeoutException | IOException e)
        {
            throw new RuntimeException(e);
        }
    }
}
