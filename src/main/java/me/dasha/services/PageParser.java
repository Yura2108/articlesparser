package me.dasha.services;

import com.rabbitmq.client.*;
import me.dasha.ArticleCollector;
import me.dasha.storage.Article;
import me.dasha.storage.LinkData;
import me.dasha.utils.RabbitUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

public class PageParser implements Runnable
{
    private static final Logger logger = LoggerFactory.getLogger(PageParser.class);
    private final ConnectionFactory factory;

    public PageParser(ConnectionFactory factory)
    {
        this.factory = factory;
    }

    @Override
    public void run()
    {
        try
        {
            Connection connection = factory.newConnection();
            var channel = connection.createChannel();
            channel.basicConsume(RabbitUtils.LINK_MQ, false, "pagesTag", new DefaultConsumer(channel)
            {
                @Override
                public void handleDelivery(
                    String consumerTag,
                   Envelope envelope,
                   AMQP.BasicProperties properties,
                   byte[] body
                ) throws IOException
                {
                    System.out.println("AHFIDHFIDSHIFH");
                    long deliveryTag = envelope.getDeliveryTag();
                    String message = new String(body, StandardCharsets.UTF_8);
                    LinkData data = LinkData.fromString(message);

                    ProcessLink(data, channel);
                    channel.basicAck(deliveryTag, false);
                }
            });
        }
        catch (TimeoutException | IOException e)
        {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    private void ProcessLink(LinkData data, Channel channel) {
        try {
            Document document = Jsoup.connect(data.getUrl()).get();
            Elements elements = document.getElementsByClass("topic-body__content-text");

            // Получаем автора, заголовок и дату публикации из HTML
            String author = document.select("meta[name=author]").attr("content");
            String title = document.select("meta[property=og:title]").attr("content");
            String publishDate = document.select("meta[property=article:published_time]").attr("content");

            Article article = new Article(author, title, publishDate, data.getUrl());

            for (Element element : elements) {
                if (element.tagName().equals("p")) {
                    article.appendText(element.text()).appendText("\n");
                }
            }

            channel.basicPublish(
                    "",
                    RabbitUtils.PAGE_MQ,
                    null,
                    article.toString().getBytes(StandardCharsets.UTF_8)
            );
        } catch (IOException ioException) {
            logger.error("Error processing link: " + data.getUrl(), ioException);
            // Добавьте логику обработки ошибки
        }
    }
}
