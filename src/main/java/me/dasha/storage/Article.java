package me.dasha.storage;

import org.json.JSONObject;

public class Article
{
    private final String author;
    private final String title;
    private final String publishDate;
    private final String url;
    private StringBuilder textBuilder = new StringBuilder();

    public Article(String author, String title, String publishDate, String url)
    {
        this.author = author;
        this.title = title;
        this.publishDate = publishDate;
        this.url = url;
    }

    public static Article fromJson(String s)
    {
        JSONObject jsonObject = new JSONObject(s);
        return new Article(
                jsonObject.getString("author"),
                jsonObject.getString("title"),
                jsonObject.getString("publishDate"),
                jsonObject.getString("url")
        );
    }

    @Override
    public String toString()
    {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("author", author);
        jsonObject.put("title", title);
        jsonObject.put("publishDate", publishDate);
        jsonObject.put("url", url);
        return jsonObject.toString();
    }

    public String getAuthor() {
    return author;
    }

    public String getTitle() {
    return title;
    }

    public String getPublishDate() {
    return publishDate;
    }

    public String getURL() {
    return url;
    }

    public String getText() {
    return textBuilder.toString();
    }

    public Article setText(String text)
    {
        this.textBuilder = new StringBuilder(text);
        return this;
    }

    public Article appendText(String text)
    {
        this.textBuilder.append(text);
        return this;
    }

    public String getHash()
    {
        return Integer.toString((url + title).hashCode());
    }
}
