package me.dasha.storage;

import org.json.JSONObject;

public class LinkData
{
    private String url;
    private String title;
    private String hash;

    public LinkData(String url, String title)
    {
        this.url = url;
        this.title = title;
        this.hash = GenerateHash();
    }

    private String GenerateHash()
    {
        return Integer.toString((url + title).hashCode());
    }

    @Override
    public String toString()
    {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("url", url);
        jsonObject.put("title", title);
        jsonObject.put("hash", hash);
        return jsonObject.toString();
    }

    public static LinkData fromString(String jsonString) {
        JSONObject jsonObject = new JSONObject(jsonString);
        String url = jsonObject.getString("url");
        String title = jsonObject.getString("title");
        return new LinkData(url, title); // hash will be automatically generated
    }

    public String getUrl() {return url;}
}
